-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------


local background = display.newImageRect( "background.png", display.contentWidth, display.contentHeight )
background.x = display.contentCenterX
background.y = display.contentCenterY

balloon = display.newImageRect( "balloon.png", 64, 64 )
balloon.x = display.contentCenterX
balloon.y = display.contentCenterY
balloon.alpha = 0.8

local topBorder = display.newImageRect( "platform.png", display.contentWidth, 50 )
	topBorder.x = display.contentCenterX
	topBorder.y = 0
	
local botBorder = display.newImageRect( "platform.png", display.contentWidth, 50 )
	botBorder.x = display.contentCenterX
	botBorder.y = display.contentHeight 

local physics = require( "physics" )
physics.start()
physics.addBody( balloon, "dynamic", { radius=32, bounce=0.3 } )
physics.addBody( topBorder, "static" )
physics.addBody( botBorder, "static" )

local function pushBalloon()
	balloon:applyLinearImpulse( 0,-0.35, balloon.x, balloon.y )
end
background:addEventListener( "tap", pushBalloon )
	
topObstacle = {}
botObstacle = {}

for i=0,2 do
	topObstacle[i] = display.newImageRect( "platform.png", 50, display.contentHeight/3 )
	topObstacle[i].x = display.contentWidth + (display.contentWidth + 2* topObstacle[i].width)/3*i
	topObstacle[i].y = display.contentHeight/6+topBorder.height/2
	physics.addBody( topObstacle[i], "static" )
	
	botObstacle[i] = display.newImageRect( "platform.png", 50, display.contentHeight/3 )
	botObstacle[i].x = display.contentWidth + (display.contentWidth + 2* botObstacle[i].width)/3*i
	botObstacle[i].y = display.contentHeight - display.contentHeight/6-botBorder.height/2
	physics.addBody( botObstacle[i], "static" )
end

local score = 0
local scoreText = display.newText( score, display.contentCenterX, display.contentCenterY, native.systemFont, 20 )
scoreText:setFillColor( 0, 0, 0 )

local function on_frame( event )
	if balloon.x < 0 then
		scoreText.text = "Finished after " .. score .. " block!"
		Runtime:removeEventListener("enterFrame", on_frame)
	else
		scoreText.text = score
	end
	for i=0,2 do
		if topObstacle[i].x < -topObstacle[i].width then
			topObstacle[i].x = display.contentWidth+topObstacle[i].width
			score = score +1
		else
			topObstacle[i].x = topObstacle[i].x-0.3
		end
		if botObstacle[i].x < -topObstacle[i].width then
			botObstacle[i].x = display.contentWidth+botObstacle[i].width
		else
			botObstacle[i].x = botObstacle[i].x-0.3
		end
	end
	local vx,vy = balloon:getLinearVelocity()
	balloon:setLinearVelocity(0,vy)
end
Runtime:addEventListener( "enterFrame", on_frame )
